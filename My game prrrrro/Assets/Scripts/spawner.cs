﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour 
    {

    public GameObject spawnee;
    public bool stopSpawning = false;
    public float spawnTime;
    public float spawnDeLay;

    private void Start()
    {
        InvokeRepeating("SpawnObject", spawnTime, spawnDeLay);

    }

    public void SpawnObject() {
        Instantiate(spawnee, transform.position, transform.rotation);
        if (stopSpawning)
        {
            CancelInvoke("spawnObject");
        }
    }

}
