﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour {

    public static int score;
    public PlayerFPSController fpsController;
    public Text finalScoreText;
    public Text inGamesScoreText;
    public GameObject gameOverWindow;


	// Use this for initialization
	void Start () {
        gameOverWindow.SetActive(false);
		
	}
	
	// Update is called once per frame
	void Update () {
        if (fpsController.playerIsDead())
        {
            finalScoreText.text = "SCORE: " + score.ToString("00000");
            gameOverWindow.SetActive(true);

            if (Input.GetKey(KeyCode.Return))
            {
                restartGame();
            }

        }
        else
        {
            inGamesScoreText.text = "SCORE: " + score.ToString("00000");
        }
		
	}
    public static void restartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
