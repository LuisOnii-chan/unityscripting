﻿    using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMovement))]
public class PlayerFPSController : MonoBehaviour {

    private CharacterMovement characterMovement;


	// Use this for initialization
	void Start () {

       
        GameObject.Find("Capsule").GetComponent<MeshRenderer>().enabled = false;
        characterMovement = GetComponent<CharacterMovement>();
		
	}
	
	// Update is called once per frame
	void Update () {
        movement();

        //Movement
        //float hMovement = Input.GetAxisRaw("Horizontal");
        //float vMovement = Input.GetAxisRaw("Vertical");

        //Vector3 movementDirection = hMovement * Vector3.right + vMovement * Vector3.forward;
        //transform.Translate(movementDirection * (walkSpeed * Time.deltaTime));

        //Rotation
        //float vCamRotation = Input.GetAxis("Mouse Y") * vRotationSpeed * Time.deltaTime;
        //float hPlayerRotation = Input.GetAxis("Mouse X") * hRotationSpeed * Time.deltaTime;
       

        //transform.Rotate(0f, hPlayerRotation, 0f);
        //camerasParent.transform.Rotate(-vCamRotation, 0f, 0f);

 		
	}
    private void movement()
    {
        //Movement
        float hMovement = Input.GetAxisRaw("Horizontal");
        float vMovement = Input.GetAxisRaw("Vertical");

        bool dash = Input.GetButton("Dash");
        bool jump = Input.GetButtonDown("Jump");

        characterMovement.moveCharacter(hMovement, vMovement, jump, dash);

        //Rotation
        
    }
}
